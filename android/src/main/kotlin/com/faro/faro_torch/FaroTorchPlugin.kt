package com.faro.faro_torch

import android.content.Context
import android.content.pm.PackageManager
import android.hardware.camera2.CameraManager
import android.os.Build
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

private const val TURN_ON_METHOD = "turnOn"
private const val TURN_OFF_METHOD = "turnOff"
private const val HAS_FLASH_METHOD = "hasFlash"

class FaroTorchPlugin(private val registrar: Registrar) : MethodCallHandler {
    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = MethodChannel(registrar.messenger(), "faro_torch")
            channel.setMethodCallHandler(FaroTorchPlugin(registrar))
        }
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        when (call.method) {
            TURN_ON_METHOD -> {
                toggleFlash(true)
                result.success(null)
            }
            TURN_OFF_METHOD -> {
                toggleFlash(false)
                result.success(null)
            }
            HAS_FLASH_METHOD -> result.success(hasFlash())
            else -> result.notImplemented()
        }
    }

    private fun toggleFlash(mustEnable: Boolean) {
        if (!hasFlash()) return
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val cameraManager = getCameraManager()
            val cameraId = cameraManager.cameraIdList.first()
            cameraManager.setTorchMode(cameraId, mustEnable)
        } else {
            throw SDKVersionException()
        }
    }

    private fun hasFlash(): Boolean {
        return registrar.context().applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
    }

    private fun getCameraManager(): CameraManager {
        return registrar.context().getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }
}

class SDKVersionException : Throwable("The device doesn't have api level 23 o newer")

