#import "FaroTorchPlugin.h"
#import <faro_torch/faro_torch-Swift.h>

@implementation FaroTorchPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFaroTorchPlugin registerWithRegistrar:registrar];
}
@end
