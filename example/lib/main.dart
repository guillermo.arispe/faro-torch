import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:faro_torch/faro_torch.dart';

void main() => runApp(FaroTorchExmapleApp());

class FaroTorchExmapleApp extends StatefulWidget {
  @override
  _FaroTorchExmapleAppState createState() => _FaroTorchExmapleAppState();
}

class _FaroTorchExmapleAppState extends State<FaroTorchExmapleApp> {
  bool _hasFlash = false;
  AndroidDeviceInfo _androidInfo;

  @override
  void initState() {
    super.initState();
    checkIfDeviceHasFlash();
    checkDeviceInfo();
  }

  Future<void> checkIfDeviceHasFlash() async {
    bool hasFlash = await FaroTorch.hasFlash();

    if (widgetWasRemove()) return;

    setState(() {
      _hasFlash = hasFlash;
    });
  }

  Future checkDeviceInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    _androidInfo = await deviceInfo.androidInfo;
  }

  bool widgetWasRemove() => !mounted;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Faro Torch Plugin example'),
        ),
        body: buildBody(),
      ),
    );
  }

  Widget buildBody() {
    return Center(
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Center(child: Text('Device has flash: $_hasFlash')),
            Center(child: Text('${_androidInfo.model}')),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      child: Text("ON"),
                      onPressed: () => turnOn(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      child: Text("OFF"),
                      onPressed: () => turnOff(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future turnOn() async {
    await FaroTorch.turnOn();
  }

  Future turnOff() async {
    await FaroTorch.turnOff();
  }
}
