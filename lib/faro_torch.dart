import 'dart:async';

import 'package:flutter/services.dart';

class FaroTorch {
  static const CHANNEL_NAME = 'faro_torch';
  static const TURN_ON_METHOD = 'turnOn';
  static const TURN_OFF_METHOD = 'turnOff';
  static const HAS_FLASH_METHOD = 'hasFlash';

  static const MethodChannel _channel = const MethodChannel(CHANNEL_NAME);

  static Future<bool> hasFlash() async {
    final bool hasFlash = await _channel.invokeMethod(HAS_FLASH_METHOD);
    return hasFlash;
  }

  static Future<void> turnOn() async {
    await _channel.invokeMethod(TURN_ON_METHOD);
  }

  static Future<void> turnOff() async {
    await _channel.invokeMethod(TURN_OFF_METHOD);
  }
}
