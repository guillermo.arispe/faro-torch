import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:faro_torch/faro_torch.dart';

void main() {
  const MethodChannel channel = MethodChannel('faro_torch');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return true;
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('hasFlash', () async {
    expect(await FaroTorch.hasFlash(), true);
  });
}
