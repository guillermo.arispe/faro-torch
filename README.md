# Faro Torch

[![pub package](https://img.shields.io/pub/v/faro_torch.svg)](https://pub.dartlang.org/packages/faro_torch)

Flutter plugin to use the camera phone flash without using the camera.

⚠️ **At the moment, it is only possible to use on Android. We are working on the implementation for iOS** 

Note: This plugin is still under development, and some APIs might not be available yet. [Feedback welcome](https://gitlab.com/guillermo.arispe/faro-torch/issues) and [Pull Requests](https://gitlab.com/guillermo.arispe/faro-torch/merge_requests) are most welcome! 

## Install

To use this plugin, add `faro_torch` as a dependency in your `pubspec.yaml` file.

### Android

Change the minimum Android sdk version to 23 (or higher) in your `android/app/build.gradle` file.

```
minSdkVersion 23
```

Add the following permissions on your `AndroidManifest.xml`:
```xml
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.FLASHLIGHT" />
<uses-feature android:name="android.hardware.camera" />
<uses-feature android:name="android.hardware.camera.autofocus" />
<uses-feature android:name="android.hardware.camera.flash" />
```

### iOS

Work in progress 🔨

## Features:

* Check if device has flash
* Turn on flash
* Turn off flash 

## Example

```dart
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:faro_torch/faro_torch.dart';

void main() => runApp(FaroTorchExmapleApp());

class FaroTorchExmapleApp extends StatefulWidget {
  @override
  _FaroTorchExmapleAppState createState() => _FaroTorchExmapleAppState();
}

class _FaroTorchExmapleAppState extends State<FaroTorchExmapleApp> {
  bool _hasFlash = false;

  @override
  void initState() {
    super.initState();
   
    checkIfDeviceHasFlash();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Faro Torch Plugin example'),
        ),
        body: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    return Center(
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      child: Text("ON"),
                      onPressed: () => _turnOn(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      child: Text("OFF"),
                      onPressed: () => _turnOff(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  
  Future<void> checkIfDeviceHasFlash() async {
    bool hasFlash = await FaroTorch.hasFlash();

    if (widgetWasRemove()) return;

    setState(() {
      _hasFlash = hasFlash;
    });
  }

  Future _turnOn() async {
    await FaroTorch.turnOn();
  }

  Future _turnOff() async {
    await FaroTorch.turnOff();
  }

  bool widgetWasRemove() => !mounted;
}
```

