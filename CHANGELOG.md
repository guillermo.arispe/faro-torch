## 0.0.3

* Update `kotlin-gradle-plugin` version to `1.3.50`

## 0.0.2

* Fix format warning using `flutter format`

## 0.0.1

* Initial release
